<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="chat.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KEAL CHAT</title>
    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
</head>

<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        if( $_REQUEST['mensaje'] != "" and $_REQUEST['nombre'] != "" ) {
            $archivo = fopen("chat.txt", "a");

            $contenido = "[".date('d-m-y][H:i:s',strtotime('now'))."] ".$_REQUEST['nombre'].": ".$_REQUEST['mensaje'];
    
            fwrite($archivo, $contenido."\n");
    
            fclose($archivo);
        }
    }
?>

<body>
    <h1 id="h1">KEAL CHAT</h1>

    <div id="divchat">
        <?php
            $lineas = array();
            $archivo = fopen("chat.txt", "r");
            
            while( !feof($archivo) ) {
                $linea = fgets($archivo);
                $lineas[] = explode(' ', $linea);
            }

            foreach( $lineas as $mostrar => $mensaje) {
                echo "<p id='p'>";
                foreach ( $mensaje as $l => $m ) {
                    echo "$m ";
                }
                echo "</p>";
            }
            
            fclose($archivo);
        ?>
    </div>
    <script>                 
        var objDiv = document.getElementById("divchat");         
        objDiv.scrollTop = objDiv.scrollHeight;             
    </script>

    <div id="divbuscarenviar">
        <form enctype="multipart/form-data" action="chat.php" method="post" name="formulario">
            <input type="text" name="nombre" placeholder="Nombre..."/>
            <input type="text" name="mensaje" size="50" placeholder="Mensaje..."/>
            <button type="submit">Enviar</button>
        </form>
    </div>
</body>
</html>